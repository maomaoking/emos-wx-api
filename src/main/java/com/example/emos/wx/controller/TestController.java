package com.example.emos.wx.controller;

import com.example.emos.wx.common.util.R;
import com.example.emos.wx.controller.form.TestSayHelloForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author liujingmao
 * @version 1.0
 * @date 2021/1/31 11:19
 */

@RestController
@RequestMapping("/test")
@Api("测试Web接口")
public class TestController {

    @PostMapping("/sayHello")
    @ApiOperation("简单的测试方法")

    public R sayHell(){
        return R.ok().put("message","HellowWorld");
    }

    @PostMapping("sayHello2")
    @ApiOperation("sayHello2方法")
    public R sayHello(@Valid @RequestBody TestSayHelloForm form){

        return R.ok().put("message","Heloo"+form.getName());


    }



}
