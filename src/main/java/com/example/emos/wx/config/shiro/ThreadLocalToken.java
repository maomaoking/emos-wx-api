package com.example.emos.wx.config.shiro;

import org.springframework.stereotype.Component;

/**
 * @author liujingmao
 * @version 1.0
 * @date 2021/1/31 22:22
 */
@Component
public class ThreadLocalToken {

    private ThreadLocal local = new ThreadLocal();

    public void setToken(String token){
        local.set(token);
    }

    public String getToken(){
        return (String) local.get();
    }

    public void clear(){
        local.remove();
    }

}
