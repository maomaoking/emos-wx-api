package com.example.emos.wx.config.xss;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 为了让刚刚定义的包装类生效，我们还要在
 * com.example.emos.wx.config.xss中创建XssFilter过滤器。
 * 过滤器拦截所有请求，然后把请求传入包装类，这样包装类就能覆盖所有请求的参数方法，
 * 用户从请求中获得数据，全都经过转义
 * @author liujingmao
 * @version 1.0
 * @date 2021/1/31 16:32
 */
public class XssFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper(
                (HttpServletRequest) request
        );
        chain.doFilter(xssRequest,response);
    }

    @Override
    public void destroy() {

    }
}
