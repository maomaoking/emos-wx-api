package com.example.emos.wx.config.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * JWT和Shiro框架对接起来，这样Shiro框架就会拦截所有的Http请求，
 * 然后验证请求提交的Token是否有效。
 * 客户端提交的Token不能直接交给Shiro框架，
 * 需要先封装成AuthenticationToken类型的对象，
 * 所以我们我们需要先创建AuthenticationToken的实现类。
 * @author liujingmao
 * @version 1.0
 * @date 2021/1/31 21:51
 */
public class OAuth2Token implements AuthenticationToken {

    private String token;

    public OAuth2Token(String token){
        this.token=token;
    }


    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
