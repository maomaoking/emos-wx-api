package com.example.emos.wx.aop;

import com.example.emos.wx.common.util.R;
import com.example.emos.wx.config.shiro.ThreadLocalToken;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 我们在写OAuth2Filter的时候，
 * 把更新后的令牌写到ThreadLocalToken里面的ThreadLocal。
 * 创建AOP切面类，拦截所有Web方法的返回值，在返回的R对象中添加更新后的令牌。
 * @author liujingmao
 * @version 1.0
 * @date 2021/1/31 23:28
 */
@Aspect
@Component
public class TokenAspect {
    @Autowired
    private ThreadLocalToken threadLocalToken;

    @Pointcut("execution(public * com.example.emos.wx.controller.*.*(..)))")
    public void aspect(){
        //TODO
    }

    @Around("aspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable{
        //方法执行结果
        R r = (R) point.proceed();
        String token = threadLocalToken.getToken();
        //如果是ThreadLocal中存在token,说明是更新的Token
        if(token!=null){
            //响应上放置Token
            r.put("token",token);
            threadLocalToken.clear();
        }
        return r;

    }
}
